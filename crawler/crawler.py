#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pyquery import PyQuery as pq
import re
import urllib
import urllib2
import sys
import codecs
import re
import json

def getIconLink(style):
	if not style:
		return ""
	m = re.search(r"\(([^}]+)\)", style)
	if not m:
		return ""
	return m.group(1)

def validate_data(data):
	print len(data["category"])
	print len(data["name"])
	print len(data["icon"])
	print len(data["hype-score"])
	print len(data["risk-score"])
	print len(data["report-score"])
	print len(data["start-date"])
	print len(data["end-date"])

	return True

def translate_score(score):
	print score
	if score == "VERY HIGH":
		return "AAA"
	elif score == "HIGH":
		return "AA"
	elif score == "MEDIUM":
		return "B"
	elif score == "LOW":
		return "C"
	elif "STABLE" in score:
		return "A"
	elif "RISKY" in score:
		return "D"
	elif "POSITVE" in score:
		return "AA"
	else:
		return "TBA"

def reformat_data(data):
	if not validate_data(data):
		return None

	formatted_data = []
	for i in xrange(20):
		ico = dict()
		ico["category"] = data["category"][i]
		ico["name"] = data["name"][i]
		ico["icon"] = data["icon"][i+1]
		ico["hype-score"] = translate_score(data["hype-score"][i])
		ico["risk-score"] = translate_score(data["risk-score"][i])
		ico["report-score"] = translate_score(data["report-score"][i])
		ico["start-date"] = data["start-date"][i]
		ico["end-date"] = data["end-date"][i]
		formatted_data.append(ico)

	return formatted_data

def getICOData(url):
	data = dict()
	data["category"] = []
	data["name"] = []
	data["icon"] = []
	data["hype-score"] = []
	data["risk-score"] = []
	data["report-score"] = []
	data["start-date"] = []
	data["end-date"] = []

	# ".ico-projects-table .ico-projects-table__row"
	d = pq(url=url)
	html_data = d('table.ico-projects-table')
	icons = html_data('tr.ico-projects-table__row')
	names = html_data('td.ico-project-name')
	hype_scores = html_data('td.ico-project-hypeScore')
	risk_scores = html_data('td.ico-project-riskScore')
	report_scores = html_data('td.ico-project-rating')
	start_dates = html_data('td.ico-project-date')
	end_dates = html_data('.ico-project-date__end')

	for name in names('.ico-project-name--description').items():
		cat_html = name.html()
		if not cat_html:
			cat = ""
		else:
			cat = cat_html.encode('utf-8').strip(' ')
		# print cat
		data["category"].append(cat)

	for name in names.items():
		_name = name.text().encode('utf-8').strip(' ')
		data["name"].append(_name.split(' ')[0])
		# print _name

	for icon in icons.items():
		style = icon('td').attr('style')
		# print style
		_icon = getIconLink(style)
		data["icon"].append(_icon)
		# print _icon

	for hype_score in hype_scores('.ico-project-wrapper').items():
		_hype_score = hype_score.text().encode('utf-8').strip(' ')
		data["hype-score"].append(_hype_score)
		# print _hype_score
		

	for risk_score in risk_scores('.ico-project-wrapper').items():
		_risk_score = risk_score.text().encode('utf-8').strip(' ')
		data["risk-score"].append(_risk_score)
		# print _risk_score

	for report_score in report_scores('span').items():
		_report_score = report_score.text().encode('utf-8').strip(' ')
		data["report-score"].append(_report_score)
		# print _report_score

	for start_date in start_dates('.ico-project-date__value').items():
		_start_date = start_date.text().encode('utf-8').strip(' ')
		data["start-date"].append(_start_date)
		# print _start_date

	for end_date in end_dates('.ico-project-date__value').items():
		_end_date = end_date.text().encode('utf-8').strip(' ')
		data["end-date"].append(_end_date)
		# print _end_date

	return reformat_data(data)

url = "http://icorating.com/"
data = getICOData(url)
print len(data)
data_json = json.dumps(data, ensure_ascii=False)
print data_json
