from calculator import get_document_score
from dict_parser import get_seeddict, parse_words
from twitter_crawler import Twitter
import json

# parameters
positive_words_path = 'data/positive.txt'
negative_words_path = 'data/negative.txt'
project_dictionary_path = 'data/dictionary.dat'
# documents = ['data/1155049829.txt', 'data/1155047854.txt', 'data/LAW_Yue_Hei.txt', 'data/Tse_Ching_Hin.txt']
docuemnts = ['data/1155047854.txt']
def calculate_score(documents, seed_dict):
	documents_scores = dict()

	# calculate score using our dictionary data
	for document in documents:
		if document in documents_scores:
			continue
		print ("\nText %s" % document)
		documents_scores[document] = get_document_score(document, seed_dict)
	# print documents_scores

	total_score = 0.0
	total_num = 0
	for key in documents_scores:
		total_score += documents_scores[key]
		total_num += 1

	print ("\n########################################")
	print ("# Sum of Score = %s" % total_score)
	print ("# Total num = %s" % total_num)
	print ("########################################\n")


# start program

twitter = Twitter()

print ("########################################")
print ("#                                      #")
print ("#     Sentiment-Analysis (Twitter)     #")
print ("#                                      #")
print ("########################################")

while(True):
	print ("Please input the option number")
	print ("1. Analyse tweets")
	print ("2. Analyse User Timeline")
	print ("3. quit")
	val = ''
	while (val != '1' and val != '2' and val != '3'):
		val = raw_input("Your Input (1, 2 or 3): ")

	if val == '1':
		tweet = raw_input("\nPlease input the tweet tag name: ")
		tweet_results = twitter.getTweets(tweet)
		with open(project_dictionary_path) as fi:
			seed_dict = json.load(fi)
			calculate_score(tweet_results, seed_dict)
		
	elif val == '2':
		username = raw_input("\nPlease input the username: ")
		user_results = twitter.getUserTimeline(username)
		with open(project_dictionary_path) as fi:
			seed_dict = json.load(fi)
			calculate_score(user_results, seed_dict)
	else:
		break
